package com.example.demospringboot.service;

import java.util.function.IntPredicate;
import java.util.stream.IntStream;

import org.springframework.stereotype.Service;
import org.xmlunit.util.IsNullPredicate;

import com.example.demospringboot.controller.EvenOdd;

@Service
public class NumberCheckService {

	public  boolean isPrime(int number) {
		IntPredicate isDivisible = index -> number % index == 0;
		return number > 1 && IntStream.range(2, number - 1).noneMatch(isDivisible);
	}

	public boolean isPalindrome(int number) {
		return number == IntStream.iterate(number, i -> i / 10).map(n -> n % 10).limit(String.valueOf(number).length())
				         .reduce(0, (a, b) -> a = a * 10 + b);
	}

	public String isOdd(int number) {
		// Lambda Expression
		EvenOdd evenOdd = (int a) -> {
			if (a % 2 == 0) {
				System.out.println("Number " + a + " is even.");
				return "Number " + a + " is even.";
			} else {
				System.out.println("Number " + a + " is odd.");
				return "Number " + a + " is odd.";
			}
		};
		return evenOdd.check(number);
	}
}
