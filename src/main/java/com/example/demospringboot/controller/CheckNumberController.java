package com.example.demospringboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.demospringboot.service.NumberCheckService;

@RestController
public class CheckNumberController {

	@Autowired
	private NumberCheckService numberCheckService;

	@GetMapping("checkOdd/{number}")
	public String isOdd(@PathVariable("number") String number1) {
		int number = 0;
		if (isNumber(number1)) {
		try {
			number = Integer.parseInt(number1);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return numberCheckService.isOdd(number);

		} else {
			return "false";
		}
	}

	@GetMapping("checkPrime/{number}")
	public boolean isPrimeMethod(@PathVariable("number") String number1) {
		int number = 0;
		try {
			number = Integer.parseInt(number1);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return numberCheckService.isPrime(number);
	}

	@GetMapping("checkPalindrome/{number}")
	public boolean isPalindromeMethod(@PathVariable("number") String number1) {
		int number = 0;

		if (isNumber(number1)) {
			try {
				number = Integer.parseInt(number1);
			} catch (Exception e) {
				// TODO: handle exception
			}
			return numberCheckService.isPalindrome(number);
		
		} else {
			return false;
		}

		
	}

	// Returns true if s is a number else false
	public boolean isNumber(String s) {
		if(s.length()>10)
			return false;
		else{
		for (int i = 0; i < s.length(); i++)
			if (Character.isDigit(s.charAt(i)) == false)
				return false;

		return true;
		}
	}
}
