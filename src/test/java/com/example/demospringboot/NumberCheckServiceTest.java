package com.example.demospringboot;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demospringboot.controller.CheckNumberController;
import com.example.demospringboot.service.NumberCheckService;

@RunWith(SpringRunner.class)
public class NumberCheckServiceTest {

	@Mock
	private NumberCheckService numberCheckService;

	// isPrime method checking with edge test case ,true test case ,false test case
	@Test
	public void isPrimeTrueTest() {
		
		when(numberCheckService.isPrime(5)).thenReturn(true);
		// test the add functionality
		Assert.assertEquals(numberCheckService.isPrime(5), true);
	}

	@Test
	public void isPrimeFalseTest() {
		
		when(numberCheckService.isPrime(4)).thenReturn(false);
		// test the add functionality
		Assert.assertEquals(numberCheckService.isPrime(4), false);
	}

	@Test
	public void isPrimeStringInputTest() {
		
		when(numberCheckService.isPrime(Integer.parseInt("test"))).thenReturn(false);
		// test the add functionality
		Assert.assertEquals(numberCheckService.isPrime(Integer.parseInt("test")), false);
	}

	@Test
	public void isPrimeNegativeInputTest() {
		
		when(numberCheckService.isPrime(Integer.parseInt("-1"))).thenReturn(false);
		// test the add functionality
		Assert.assertEquals(numberCheckService.isPrime(Integer.parseInt("-1")), false);
	}

	@Test
	public void isPrimeInfinityInputTest() {
		
		when(numberCheckService.isPrime(Integer.parseInt("111111111111111"))).thenReturn(false);
		// test the add functionality
		Assert.assertEquals(numberCheckService.isPrime(Integer.parseInt("1111111111111111111")), false);
	}
	// isPrime method checking with edge test case ,true test case ,false test
	// case

	// isPalindrome method checking with edge test case ,true test case ,false test case
	@Test
	public void isPalindromeTrueTest() {
		
		when(numberCheckService.isPalindrome(131)).thenReturn(true);
		// test the isOdd functionality
		Assert.assertEquals(numberCheckService.isPalindrome(5), true);
	}

	@Test
	public void isPalindromeFalseTest() {
		
		when(numberCheckService.isPalindrome(132)).thenReturn(false);
		// test the isOdd functionality
		Assert.assertEquals(numberCheckService.isPalindrome(4), false);
	}

	@Test
	public void isPalindromeStringInputTest() {
		
		when(numberCheckService.isPalindrome(Integer.parseInt("test"))).thenReturn(false);
		// test the isOdd functionality
		Assert.assertEquals(numberCheckService.isPalindrome(Integer.parseInt("test")), false);
	}

	@Test
	public void isPalindromeNegativeInputTest() {
		
		when(numberCheckService.isPalindrome(Integer.parseInt("-1"))).thenReturn(false);
		// test the isOdd functionality
		Assert.assertEquals(numberCheckService.isPalindrome(Integer.parseInt("-1")), false);
	}

	@Test
	public void isPalindromeInfinityInputTest() {
		
		when(numberCheckService.isPalindrome(Integer.parseInt("111111111111111"))).thenReturn(false);
		// test the isOdd functionality
		Assert.assertEquals(numberCheckService.isPalindrome(Integer.parseInt("1111111111111111111")), false);
	}

	// isPalindrome method checking with edge test case ,true test case ,false test case
	
	// isOdd method checking with edge test case ,true test case ,false test case
	@Test
	public void isOddTrueTest() {
		
		when(numberCheckService.isOdd(4)).thenReturn("EVEN");
		// test the isOdd functionality
		Assert.assertEquals(numberCheckService.isOdd(5), true);
	}

	@Test
	public void isOddFalseTest() {
		
		when(numberCheckService.isOdd(3)).thenReturn("Odd");
		// test the isOdd functionality
		Assert.assertEquals(numberCheckService.isOdd(4), false);
	}

	@Test
	public void isOddStringInputTest() {
		
		when(numberCheckService.isOdd(Integer.parseInt("test"))).thenReturn("false");
		// test the isOdd functionality
		Assert.assertEquals(numberCheckService.isOdd(Integer.parseInt("test")), false);
	}

	@Test
	public void isOddNegativeInputTest() {
		
		when(numberCheckService.isOdd(Integer.parseInt("-1"))).thenReturn("false");
		// test the isOdd functionality
		Assert.assertEquals(numberCheckService.isOdd(Integer.parseInt("-1")), false);
	}

	@Test
	public void isOddInfinityInputTest() {
		
		when(numberCheckService.isOdd(Integer.parseInt("111111111111111"))).thenReturn("false");
		// test the isOdd functionality
		Assert.assertEquals(numberCheckService.isOdd(Integer.parseInt("1111111111111111111")), false);
	}
	// isOdd method checking with edge test case ,true test case ,false test case
}
