package com.example.demospringboot;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Any;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demospringboot.controller.CheckNumberController;
import com.example.demospringboot.service.NumberCheckService;

@RunWith(SpringRunner.class)
public class CheckNumberControllerTest {
	
	@InjectMocks
	private CheckNumberController checkNumberController;
	
	@Mock
	private NumberCheckService numberCheckService;
	
	
	// isOdd method checking with edge test case ,true test case ,false test case 
	@Test
	public void isOddTestEven() throws Exception{
		
		//add the behavior to even numbers
	      when(numberCheckService.isOdd(10)).thenReturn(" EVEN ");
	    //test the even functionality
	      Assert.assertEquals(checkNumberController.isOdd("10")," EVEN ");
	}
	
	@Test
	public void isOddTestOdd() throws Exception{
		
		//add the behavior to odd numbers
	      when(numberCheckService.isOdd(5)).thenReturn(" Odd ");
	    //test the odd functionality
	      Assert.assertEquals(checkNumberController.isOdd("5")," Odd ");
	}
	
	@Test
	public void isOddStringInputTest() throws Exception{
		
		//add the behavior to odd numbers
	      when(numberCheckService.isOdd(Integer.parseInt("test"))).thenReturn(" false ");
	    //test the odd functionality
	      Assert.assertEquals(checkNumberController.isOdd("test")," false ");
	}
	
	@Test
	public void isOddNegativeInputTest() throws Exception{
		
		//add the behavior to odd numbers
	      when(numberCheckService.isOdd(Integer.parseInt("-1"))).thenReturn(" false ");
	    //test the odd functionality
	      Assert.assertEquals(checkNumberController.isOdd("-1")," false ");
	}
	
	@Test
	public void isOddInfinityInputTest() throws Exception{
		
		//add the behavior to odd numbers
	      when(numberCheckService.isOdd(Integer.parseInt("-1"))).thenReturn(" false ");
	    //test the odd functionality
	      Assert.assertEquals(checkNumberController.isOdd("-1")," false ");
	}
	
	// isOdd method checking with edge test case ,true test case ,false test case 
	
	
	// isPrime method checking with edge test case ,true test case ,false test case 
	@Test
	public void isPrimeTrueTest() throws Exception{
		
		//add the behavior to prime numbers
	      when(numberCheckService.isPrime(5)).thenReturn(true);
	    //test the prime functionality
	      Assert.assertEquals(checkNumberController.isPrimeMethod("5"),true);
	}
	
	@Test
	public void isPrimeFalseTest() throws Exception{
		
		//add the behavior to prime numbers
	      when(numberCheckService.isPrime(2)).thenReturn(false);
	    //test the prime functionality
	      Assert.assertEquals(checkNumberController.isPrimeMethod("2"),false);
	}
	
	@Test
	public void isPrimeStringInputTest() throws Exception{
		
		//add the behavior to prime numbers
	      when(numberCheckService.isPrime(Integer.parseInt("test"))).thenReturn(false);
	    //test the prime functionality
	      Assert.assertEquals(checkNumberController.isPrimeMethod("test"),false);
	}
	@Test
	public void isPrimeNegativeInputTest() throws Exception{
		
		//add the behavior to prime numbers
	      when(numberCheckService.isPrime(Integer.parseInt("-1"))).thenReturn(false);
	    //test the prime functionality
	      Assert.assertEquals(checkNumberController.isPrimeMethod("-1"),false);
	}
	
	@Test
	public void isPrimeInfinityInputTest() throws Exception{
		
		//add the behavior to prime numbers
	      when(numberCheckService.isPrime(Integer.parseInt("111111111111111111"))).thenReturn(false);
	    //test the prime functionality
	      Assert.assertEquals(checkNumberController.isPrimeMethod("111111111111111111"),false);
	}
	// isPrime method checking with edge test case ,true test case ,false test case
	
	// isPalindrome method checking with edge test case ,true test case ,false test case
	@Test
	public void isPalindromeTrueTest() throws Exception{
		
		//add the behavior to palindrome numbers
	      when(numberCheckService.isPalindrome(131)).thenReturn(true);
	    //test the prime functionality
	      Assert.assertEquals(checkNumberController.isPalindromeMethod("131"),true);
	}

	@Test
	public void isPalindromeFalseTest() throws Exception{
		
		//add the behavior to palindrome numbers
	      when(numberCheckService.isPalindrome(132)).thenReturn(false);
	    //test the prime functionality
	      Assert.assertEquals(checkNumberController.isPalindromeMethod("132"),false);
	}
	@Test
	public void isPalindromeStringInputTest() throws Exception{
		
		//add the behavior to palindrome numbers
	      when(numberCheckService.isPalindrome(Integer.parseInt("test"))).thenReturn(false);
	    //test the prime functionality
	      Assert.assertEquals(checkNumberController.isPalindromeMethod("test"),false);
	}
	@Test
	public void isPalindromeNegativeInputTest() throws Exception{
		
		//add the behavior to palindrome numbers
	      when(numberCheckService.isPalindrome(Integer.parseInt("-1"))).thenReturn(false);
	    //test the prime functionality
	      Assert.assertEquals(checkNumberController.isPalindromeMethod("-1"),false);
	}
	@Test
	public void isPalindromeInfinityInputTest() throws Exception{
		
		//add the behavior to palindrome numbers
	      when(numberCheckService.isPalindrome(Integer.parseInt("1111111111111111111"))).thenReturn(false);
	    //test the prime functionality
	      Assert.assertEquals(checkNumberController.isPalindromeMethod("1111111111111111111"),false);
	}
	// isPalindrome method checking with edge test case ,true test case ,false test case
}
